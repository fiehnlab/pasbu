require_relative "open_babel_helper"
require 'json'

class ProgramModule
	def initialize(temp_dir, output_dir, config_dir, program_dir)
    @output_dir = output_dir
    @file_count = 0
    @structure_file_set = {}
  end

  def new_temp_dir() false end
  def new_output_dir() false end

	def calculate(spectrum, file_suffix)
		# wait as long as another thread calculates the same compound
		while @structure_file_set[spectrum[:inchi]] == "calculating"
			sleep 0.3
		end

		# calculation only needs to be done if it has not happened laready for this compound
		if !(fragments = @structure_file_set[spectrum[:inchi]])
			@file_count += 1

			@structure_file_set[spectrum[:inchi]] = "calculating"
			output_file = get_output_file_name @file_count

			@structure_file_set[spectrum[:inchi]] = fragments = File.exist?(output_file) ? File.open(output_file, "rb").read.scan(/(\s{3}Mass\sFrontier[\s\n\d.A-Z]*\${4}\s)/).map(&:first).map do |fragment|
				{
					"structure" => OpenBabelHelper.convert_standard(fragment, "sdf"),
					"mass" => OpenBabelHelper.get_mass(fragment, "sdf")
				}
			end : []

			File.open(get_output_file_name(@file_count, 1), "w") { |f| f.write fragments.to_json }
		end

		process_data spectrum, file_suffix
	end

	def process_data(spectrum, file_suffix)
		while @structure_file_set[spectrum[:inchi]] == "calculating"
			sleep 0.3
		end

		if !(fragments = @structure_file_set[spectrum[:inchi]])
			@file_count += 1

			@structure_file_set[spectrum[:inchi]] = "calculating"
			output_file = get_output_file_name @file_count, 1

			@structure_file_set[spectrum[:inchi]] = fragments = File.exist?(output_file) ? JSON.parse(File.open(output_file, "rb").read) : nil
		end

		if fragments
			spectrum[:peaks].each do |peak|
				fragments.each do |fragment|
					fragment["assigned_to_peak"] = false;
					delta_mass = (peak[:mz] - fragment["mass"].to_f).abs

					if delta_mass <= 0.005
						if !peak[:structure][:massfrontier] || delta_mass < (peak[:mz] - peak[:structure][:massfrontier]["mass"]).abs
							peak[:structure][:massfrontier]["assigned_to_peak"] = false if peak[:structure][:massfrontier]
							fragment["assigned_to_peak"] = true
							peak[:structure][:massfrontier] = fragment
						end
					end
				end

				peak[:structure][:massfrontier] = peak[:structure][:massfrontier]["structure"] if peak[:structure][:massfrontier]
			end

			# unused fragments -> FP (confusion matrix)
			fragments.each do |fragment|
				if !fragment["assigned_to_peak"]
					spectrum[:peaks] << {
						:structure => {
							:massfrontier => fragment["structure"]
						}
					}
				end
			end
		end

		spectrum
	end

	def get_output_file_name(file_suffix, mode = 0)
		mode == 0 ? "#{@output_dir}\\__#{file_suffix.to_i}.sdf" : "#{@output_dir}\\computed_#{file_suffix.to_i}.json"
	end
end
