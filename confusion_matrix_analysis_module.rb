class ConfusionMatrixAnalysisModule
	def self.analyze(spectra, program)
		result_string = "SPECTRA_NUM	PEAK_NUM	MW	TP	TN	FP	FN\n"

		i = 1
		spectra.each do |spectrum|
			tp = 0
			tn = 0
			fp = 0
			fn = 0

			spectrum[:peaks].each do |peak|
				tp += 1 if peak[:mz] 	&& peak[:structure][program.to_sym]
				tn += 0
				fp += 1 if !peak[:mz]	&& peak[:structure][program.to_sym]
				fn += 1 if peak[:mz]	&& !peak[:structure][program.to_sym]
			end

			result_string += "#{i}	#{spectrum[:peaks].length}	#{spectrum[:precursor_mz].to_f - 1}	#{tp}	#{tn}	#{fp}	#{fn}\n"
			i += 1
		end

		result_string
	end
end
