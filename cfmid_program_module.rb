require_relative "open_babel_helper"

class ProgramModule
	def initialize(temp_dir, output_dir, config_dir, program_dir)
    @temp_dir = temp_dir
    @output_dir = output_dir
    @config_dir = config_dir
    @program_dir = program_dir
  end

  def new_temp_dir() true end
  def new_output_dir() true end

	def calculate(spectrum, file_suffix)
		if spectrum[:precursor_type] == "[M+H]+"
			spectrum_file_name = "#{@temp_dir}\\spectrum_#{file_suffix}.txt"
			output_file_name = get_output_file_name file_suffix
			config_file_name = "#{@config_dir}\\cfm_param_config.txt"
			trained_model_file_name = "#{@config_dir}\\cfm_trained_model.txt"
			
			spectrum_string = spectrum[:peaks].map { |peak| "#{peak[:mz]}	#{peak[:intensity]}" }.join("\n")

			File.open(spectrum_file_name, "w") do |f| 
				f.write "energy0\n#{spectrum_string}"
				f.write "\nenergy1\nenergy2\n"
			end

			cfm_output = `\"#{@program_dir}cfm-annotate.exe\" \"#{spectrum[:inchi] || spectrum[:smiles]}\" #{spectrum_file_name} 10 0.005 \"#{trained_model_file_name}\" \"#{config_file_name}\"`

			File.open(output_file_name, "w") do |f|
				f.write "STRUCTURE: #{spectrum[:inchi] || spectrum[:smiles]}\n"
				f.write cfm_output
			end
		end

		process_data spectrum, file_suffix
	end

	def process_data(spectrum, file_suffix)
		if File.exist? get_output_file_name(file_suffix)
			cfm_output = File.open(get_output_file_name(file_suffix), "rb").read

			if cfm_output.split("energy").count >= 2
				fragments = cfm_output.scan(/(^\d+\s\d+.\d+\s.+\n)/).map(&:first).map do |fragment_string|
					{
						:structure => fragment_string.split(" ")[2],
						:mass => fragment_string.split(" ")[1].to_f,
						:assigned_to_peak => false
					}
				end

				spectrum[:peaks].each do |peak|
					fragments.each do |fragment|
						delta_mass = (peak[:mz] - fragment[:mass]).abs

						if delta_mass <= 0.005
							if !peak[:structure][:cfmid] || delta_mass < (peak[:mz] - peak[:structure][:cfmid][:mass]).abs
								peak[:structure][:cfmid][:assigned_to_peak] = false if peak[:structure][:cfmid]
								fragment[:assigned_to_peak] = true
								peak[:structure][:cfmid] = fragment
							end
						end
					end

					peak[:structure][:cfmid] = OpenBabelHelper.convert_standard(peak[:structure][:cfmid][:structure], "smiles") if peak[:structure][:cfmid]
				end
				
				# unused fragments -> FP (confusion matrix)
				fragments.each do |fragment|
					if !fragment[:assigned_to_peak]
						spectrum[:peaks] << {
							:structure => {
								:cfmid => OpenBabelHelper.convert_standard(fragment[:structure], "smiles")
							}
						}
					end
				end
			end
		end

		spectrum
	end

	def get_output_file_name(file_suffix)
		"#{@output_dir}\\output_#{file_suffix}.txt"
	end
end
