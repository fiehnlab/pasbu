require_relative "open_babel_helper"
require 'sqlite3'

class ProgramModule
	def initialize(temp_dir, output_dir, config_dir, program_dir)
    @temp_dir = temp_dir
    @output_dir = output_dir
    @program_dir = program_dir
    @ion_modes = {"[M+H]+" => 1, "[M-H]-" => -1}
  end

  def new_temp_dir() true end
  def new_output_dir() true end

	def calculate(spectrum, file_suffix)
		if (ion_mode = @ion_modes[spectrum[:precursor_type]])
			db_file_name = get_output_file_name file_suffix
			ms_file_name = "#{@temp_dir}\\ms_data_#{file_suffix}.txt"
			structure_file_name = "#{@temp_dir}\\structure_#{file_suffix}.txt"

			File.open(structure_file_name, "w") { |f| f.write spectrum[:smiles] }

			File.open(ms_file_name, "w") do |f| 
				f.write "#{spectrum[:precursor_mz]}: 1000 (\n" 
				f.write spectrum[:peaks].map { |peak| "#{peak[:mz]}: #{peak[:intensity]}" }.join(",\n")
				f.write "\n)" 
			end

			`#{@program_dir}magma read_ms_data #{ms_file_name} #{db_file_name} -f mass_tree -p 10 -q 0.005 --precursor_mz_precision 1.0 -i #{ion_mode} 2>nul`
			`#{@program_dir}magma add_structures #{structure_file_name} #{db_file_name} 2>nul`
			`#{@program_dir}magma annotate #{db_file_name} -c 0 -d 0 2>nul`
		end

		process_data spectrum, file_suffix
	end

	def process_data(spectrum, file_suffix)
		if File.exist? get_output_file_name(file_suffix)
			db = SQLite3::Database.new get_output_file_name(file_suffix)
			annotated_peaks = db.execute "SELECT fragments.mz, fragments.smiles FROM fragments JOIN peaks on fragments.mz=peaks.mz AND fragments.scanid=peaks.scanid WHERE fragments.scanid=2 AND score<=2.0;"
			db.close

			annotated_peaks.each do |result_peak|
				spectrum[:peaks].each do |peak|
					peak[:structure][:magma] = OpenBabelHelper.convert_standard(result_peak[1], "smiles") if peak[:mz] == result_peak[0].to_f
				end
			end
		end
		
		spectrum
	end

	def get_output_file_name(file_suffix)
		"#{@output_dir}\\output_#{file_suffix}.db"
	end
end
