# example command: ruby pasbu.rb c example_spectra_list.txt cfmid,massfrontier,midas,metfrag,magma programs\cfmid "" programs\midas programs\metfrag programs\magma
# example command: ruby pasbu.rb c example_spectra_list.txt magma "programs\magma"
# example command: ruby pasbu.rb a example_spectra_list.txt cfmid,massfrontier,midas,metfrag

require 'fileutils'
require_relative 'quantitative_annotation_analysis_module'
require_relative 'confusion_matrix_analysis_module'
require_relative 'qualitative_overlap_analysis_module'

Thread.abort_on_exception = true

# config
SPECTRA_NUM = -1 # -1 for all spectra we can find
THREAD_NUM = 6 # remember: ruby itself has no real parallelism. running openbabel and other programs (see `command`) will benefit from multiple threads though
RESULTS_DIR = "results"

c_a_mode = ARGV[0]
spectra_file_list_file = ARGV[1]
programs = ARGV[2].split ","
program_dirs = ARGV[3..-1]

if !['a', 'c'].include?(c_a_mode) || (ARGV.length != 3 + programs.count && c_a_mode == "c") || (ARGV.length < 3 && c_a_mode == "a")
	abort("\nWrong number of arguments: <c/a (calculate or just analyse)> <spectra file list file> <program(s) to test (seperate with comma ',')> <if c mode: program installation directory(s seperate with space ' ')>") 
end

puts "\n\n+++ PASBU by Tobias Wermuth at UC Davis +++"
puts "\n--- #{programs.join ', '} Benchmark ---\n\n"


obabel_dir = "obabel_temp"
# can take some time depending on number of files and disk IO speed
FileUtils.rm_r obabel_dir if File.directory? obabel_dir
FileUtils.mkdir_p obabel_dir
while !File.directory? obabel_dir
	sleep 0.5
end

spectra_file_list = []
File.open(spectra_file_list_file, "r").each_line do |line|
  spectra_file_list << line.strip if line.strip != ""
end
puts "-- Spectra files --\n#{spectra_file_list.join "\n"}\n-- \\Spectra files\\ --\n\n"

spectra = []
spectra_file_list.each do |spectra_file|
	puts "Loading file (#{spectra_file}) content..."
	file_content = File.open(spectra_file, "r").read
	
	spectra_complex_list = file_content.split "\n\n"
	spectra_complex_list = spectra_complex_list.shift SPECTRA_NUM if SPECTRA_NUM != -1

	spectra_complex_list.each do |spectrum_complex|
		name = /NAME: (.*)/.match spectrum_complex
		name = name[1] if name
		precursor_type = /PRECURSORTYPE: (.*)/.match spectrum_complex
		precursor_type = precursor_type ? precursor_type[1] : "[M+H]+"
		precursor_mz = /PRECURSORMZ: (.*)/.match spectrum_complex
		precursor_mz = precursor_mz[1].to_f if precursor_mz
		inchi = /INCHI: (.*)/.match spectrum_complex
		inchi = inchi[1] if inchi
		smiles = /SMILES: (.*)/.match spectrum_complex
		smiles = smiles[1] if smiles

		peaks = spectrum_complex.scan(/(^[\d\.]+\s+\d+$)/).map do |peak|
			values = peak[0].split "	"
			peak = { :mz => values[0].to_f, :intensity => values[1].to_f, :structure => {} }
			programs.each { |program| peak[:structure][program.to_sym] = nil }
			peak
		end

		spectra << {
			:name => name,
			:precursor_mz => precursor_mz,
			:precursor_type => precursor_type,
			:inchi => inchi,
			:smiles => smiles,
			:peaks => peaks
		} if precursor_mz && (inchi || smiles) && peaks
	end

	puts "Done.\n\n"
end

program_count = 0
programs.each do |program|
	program_dir = program_dirs[program_count]
	program_dir = "#{program_dir}\\" if !program_dir.end_with?("\\")
	temp_dir = "#{program}_temp"
	output_dir = "#{program}_output"
	config_dir = "#{program}_config"

	require_relative "#{program}_program_module"
	program_module = ProgramModule.new temp_dir, output_dir, config_dir, program_dir

	if program_module.new_temp_dir
		FileUtils.rm_r temp_dir if File.directory? temp_dir
		sleep 1.0
		FileUtils.mkdir_p temp_dir
	end

	if c_a_mode == "c" && program_module.new_output_dir
		FileUtils.rm_r output_dir if File.directory?(output_dir)
		sleep 1.0
		FileUtils.mkdir_p output_dir
	end

	threads = []
	calc_time_sum = 0
	calc_time_max = 0
	count = 0

	print "Computating #{spectra.count} spectra using #{program}...\n\nProgress: "
	print animation_string = "00%    "

	spectra.map do |spectrum|
		while threads.length >= THREAD_NUM
			threads.each do |thread|
				threads = threads - [thread] if !thread.alive?
			end
			sleep 0.2
		end

		threads << Thread.new do
			count += 1
			count_string = count.to_s.rjust(5, "0")

			start_time = Time.now

			if c_a_mode == "c"
				spectrum = program_module.calculate spectrum, count_string
			elsif c_a_mode == "a"
				spectrum = program_module.process_data spectrum, count_string
			end

			calc_time = Time.now - start_time
			calc_time_sum += calc_time
			calc_time_max = calc_time if calc_time > calc_time_max
		end

		sleep 0.1

		# draw live UI
		loading_indicator = ""
		((count % 3) + 1).times { loading_indicator += "." }
		animation_string.length.times { print "\b" }
		print animation_string = "#{(count.to_f * 100 / spectra.count).round(0).to_s.rjust(2, "0")}% (#{threads.count} Threads, #{(calc_time_sum / (count + 1)).to_f.round(2)}s avg calc time, #{spectra.count - count} spectra left, #{((spectra.count - count) * (calc_time_sum / (count + 1)).to_f / THREAD_NUM / 60).to_i}min left) #{loading_indicator.rjust(3, " ")}"

		spectrum
	end

	threads.each { |thread| thread.join }
	puts "\n\nDone.\n\n"

	# annotation analysis
	quantitative_annotation_result_string = QuantitativeAnnotationAnalysisModule.analyze spectra, program, calc_time_sum, calc_time_max
	puts quantitative_annotation_result_string
	File.open("#{RESULTS_DIR}\\#{program}_results.txt", "w").write quantitative_annotation_result_string

	# confusion matrix analysis
	confusion_matrix_result_string = ConfusionMatrixAnalysisModule.analyze spectra, program
	File.open("#{RESULTS_DIR}\\#{program}_confusion_matrix_results.txt", "w").write confusion_matrix_result_string

	# remove FP peaks
	spectra.each { |spectrum| spectrum[:peaks].delete_if { |peak| !peak[:mz] } }

	program_count += 1
end

if programs.count >= 2
	qualitative_overlap_result_string = QualitativeOverlapAnalysisModule.analyze spectra, programs
	puts qualitative_overlap_result_string
	File.open("#{RESULTS_DIR}\\#{programs.join ','}_overlap_results.txt", "w").write qualitative_overlap_result_string
end

puts "\n-- Done --"

puts "\nCleaning up..."
sleep 10 # openbabel needs some seconds to end all its processes
FileUtils.rm_r obabel_dir if File.directory? obabel_dir
puts "Done"
