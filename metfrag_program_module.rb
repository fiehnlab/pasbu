require_relative "open_babel_helper"

class ProgramModule
	def initialize(temp_dir, output_dir, config_dir, program_dir)
    @temp_dir = temp_dir
    @output_dir = output_dir
    @program_dir = program_dir
    @ion_modes = {"[M+H]+" => 3, "[M-H]-" => 1}
  end

  def new_temp_dir() true end
  def new_output_dir() true end

	def calculate(spectrum, file_suffix)
		if (ion_mode = @ion_modes[spectrum[:precursor_type]])
			database_file_name = "#{@temp_dir}\\db_#{file_suffix}.sdf"
			spectrum_file_name = "#{@temp_dir}\\spectrum_#{file_suffix}.txt"
			output_file_name = get_output_file_name file_suffix

			database = OpenBabelHelper.convert spectrum[:inchi], "inchi", "sdf"

			if database.split("\n").length <= 5
				database = OpenBabelHelper.convert spectrum[:smiles], "smiles", "sdf"
			end

			File.open(database_file_name, "w") { |f| f.write database }

			File.open(spectrum_file_name, "w") do |f| 
				f.write "# Parent Mass: #{spectrum[:precursor_mz]}\n"
				f.write "# Mode: 3\n# Charge: 1\n"
				f.write spectrum[:peaks].map { |peak| "#{peak[:mz]} #{peak[:intensity]}" }.join("\n")
			end

			`java -Djava.util.Arrays.useLegacyMergeSort=true -jar #{@program_dir}metfrag.jar -a 0.005 -B -M #{ion_mode} -S #{file_suffix} -D #{spectrum_file_name} -d sdf -L #{database_file_name} -R #{@output_dir} -F -T 1`
		end

		process_data spectrum, file_suffix
	end

	def process_data(spectrum, file_suffix)
		if File.exist? get_output_file_name(file_suffix)
			result_peaks = File.open(get_output_file_name(file_suffix), "rb").read.scan /(^[\s\d\nA-Z.]*> <PeakMass>\s\n([\d.]*)[\s\n]*\${4})/

			result_peaks.each do |result_peak|
				structure = OpenBabelHelper.convert_standard(result_peak[0], "sdf")

				spectrum[:peaks].each do |peak|
					peak[:structure][:metfrag] = structure if peak[:mz] == result_peak[1].to_f
				end
			end
		end

		spectrum
	end

	def get_output_file_name(file_suffix)
		"#{@output_dir}\\#{file_suffix}_0_fragments.sdf"
	end
end
