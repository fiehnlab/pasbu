# Welcome to PASBU #
![icon.PNG](https://bytebucket.org/fiehnlab/pasbu/raw/2fe167479bab8c8343cc4805f9d7bbdfec209443/icon.PNG)
### The Peak Annotation Software Benchmark Utility ###

## Setup ##
* clone repository
* install programs you want to test
* * currently supported: CFM ID, MAGMa, MIDAS, MetFrag
* get spectra in .msp format
* list .msp files in extra text file (one line per file) (see example_spectra_list.txt)

## Running the PASBU ##
* command: <c/a (calculate or just analyse)> <spectra file list file> <program(s) to test (seperate with comma ',')> <if c mode: program installation directory(s seperate with space ' ')>
* example: ruby pasbu.rb c spectra_list.txt magma,cfmid "programs\magma" "programs\cfmid"
* example: ruby pasbu.rb a spectra_list.txt massfrontier,midas,magma

* in "c" mode pasbu will use the specified program to annotate the peaks and then run the same analysis as in "a" mode
* in "a" mode pasbu will use its internal algorithm to analyze the results. It will use the previously calculated results in the <program>_output folder for the spectra specified in the list file.

* all results will be stored in text files

## Code ##
* all code is written in Ruby (get it here [http://rubyinstaller.org/](http://rubyinstaller.org/))
* main file is pasbu.rb
* * reads and prepares spectra data
* * controls data flow
* * connects other parts of program
* program modules
* * each supported program needs/has one
* * prepare spectra data for specific program, execute program and give data back to main class
* analysis module
* * uses data to calculate certain information (quantitative, qualitative, ...)
* * always gives back a result string for the main class