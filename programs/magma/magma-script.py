#!C:\FiehnLab-Local\Tobias-Wermuth\Programs\Python\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'Magma==1.2','console_scripts','magma'
__requires__ = 'Magma==1.2'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('Magma==1.2', 'console_scripts', 'magma')()
    )
