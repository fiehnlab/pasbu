#!C:\FiehnLab-Local\Tobias-Wermuth\Programs\Python\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'MAGMaWeb==0.0','console_scripts','magma-web'
__requires__ = 'MAGMaWeb==0.0'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('MAGMaWeb==0.0', 'console_scripts', 'magma-web')()
    )
