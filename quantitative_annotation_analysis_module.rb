class QuantitativeAnnotationAnalysisModule
	def self.analyze(spectra, program, calc_time_sum, calc_time_max)
		peak_results = {
			:sum => {
				:_0_10 => 0,
				:_10_50 => 0,
				:_50_80 => 0,
				:_80_100 => 0,
			},
			:assigned => {
				:_0_10 => 0,
				:_10_50 => 0,
				:_50_80 => 0,
				:_80_100 => 0,
			}
		}

		spectra.each do |spectrum|
			spectrum[:peaks].each do |peak|
				if peak[:mz]
					case peak[:intensity] / 1000
					when 0..0.1
					  peak_results[:sum][:_0_10] += 1
					  peak_results[:assigned][:_0_10] += 1 if peak[:structure][program.to_sym]
					when 0.1..0.5
					  peak_results[:sum][:_10_50] += 1
					  peak_results[:assigned][:_10_50] += 1 if peak[:structure][program.to_sym]
					when 0.5..0.8
					  peak_results[:sum][:_50_80] += 1
					  peak_results[:assigned][:_50_80] += 1 if peak[:structure][program.to_sym]
					else
					  peak_results[:sum][:_80_100] += 1
					  peak_results[:assigned][:_80_100] += 1 if peak[:structure][program.to_sym]
					end
				end
			end
		end

		overall_peaks_0_10_rel = (peak_results[:assigned][:_0_10].to_f / peak_results[:sum][:_0_10])
		overall_peaks_10_50_rel = (peak_results[:assigned][:_10_50].to_f / peak_results[:sum][:_10_50])
		overall_peaks_50_80_rel = (peak_results[:assigned][:_50_80].to_f / peak_results[:sum][:_50_80])
		overall_peaks_80_100_rel = (peak_results[:assigned][:_80_100].to_f / peak_results[:sum][:_80_100])
		overall_peaks_rel = (
			(peak_results[:assigned][:_0_10] + peak_results[:assigned][:_10_50] + peak_results[:assigned][:_50_80] + peak_results[:assigned][:_80_100]).to_f / 
			(peak_results[:sum][:_0_10] + peak_results[:sum][:_10_50] + peak_results[:sum][:_50_80] + peak_results[:sum][:_80_100])
		)

		result_string =  "-- Overall #{program} Results --\n"
		result_string += "Percentage of peaks assigned to fragments (0-10|10-50|50-80|80-100|all): #{(overall_peaks_0_10_rel * 100).round 1}%|#{(overall_peaks_10_50_rel * 100).round 1}%|#{(overall_peaks_50_80_rel * 100).round 1}%|#{(overall_peaks_80_100_rel * 100).round 1}%|#{(overall_peaks_rel * 100).round 1}%\n"
		result_string += "Average calculation time: #{calc_time_sum / spectra.count}s  Max calculation time: #{calc_time_max}s\n"
		result_string += "-- \\Overall #{program} Results\\ --\n\n"

		result_string
	end
end
