require_relative "open_babel_helper"

class ProgramModule
	def initialize(temp_dir, output_dir, config_dir, program_dir)
    @temp_dir = temp_dir
    @output_dir = output_dir
    @config_dir = config_dir
    @program_dir = program_dir
    @ion_modes = {"[M+H]+" => "positive", "[M-H]-" => "negative"}

    default_config_file_name = "#{@config_dir}\\midas_config.cfg"
    @default_config_string = File.open(default_config_file_name, "rb").read
  end

  def new_temp_dir() true end
  def new_output_dir() true end

	def calculate(spectrum, file_suffix)
		if (ion_mode = @ion_modes[spectrum[:precursor_type]])
			spectrum_file_name = "#{@temp_dir}\\spectrum_#{file_suffix}.FT2"
			database_file_name = "#{@temp_dir}\\temp_#{file_suffix}.mdb"
			output_file_name = get_output_file_name file_suffix
			config_file_name = "#{@config_dir}\\midas_config_#{file_suffix}.cfg"

			File.open(config_file_name, "w") do |f| 
				f.write @default_config_string.sub("{METABOLITE_DATABASE_PLACEHOLDER}", database_file_name).sub("{POLARITY_PLACEHOLDER}", ion_mode)
			end

			File.open(database_file_name, "w") do |f| 
				f.write("Identifier	Name	InChI	Links\n")
				f.write("mol	mol	#{spectrum[:inchi]}	.")
			end
			
			File.open(spectrum_file_name, "w") do |f| 
				f.write("S	0	0	#{spectrum[:precursor_mz]}\n")
				f.write("Z	1	#{spectrum[:precursor_mz]}\n")
				f.write(spectrum[:peaks].map { |peak| "#{peak[:mz]}	#{peak[:intensity]}	0	0	0	0" }.join("\n"))
			end

			`python \"#{@program_dir}MIDAS.py\" -c #{config_file_name} -a #{output_file_name} -f #{spectrum_file_name}`

			File.delete config_file_name
		end

		process_data spectrum, file_suffix
	end

	def process_data(spectrum, file_suffix)
		if File.exist? get_output_file_name(file_suffix)
			result_peaks = File.open(get_output_file_name(file_suffix), "rb").read.scan /^(\d{1,4}.\d+\s)(\d{1,4}.\d+\s)[\d\s]+([\S]+)/

			result_peaks.each do |result_peak|
				spectrum[:peaks].each do |peak|
					peak[:structure][:midas] = OpenBabelHelper.convert_standard(result_peak[2], "smi") if peak[:mz] == result_peak[0].to_f && result_peak[2] != "NA"
				end
			end
		end
		
		spectrum
	end

	def get_output_file_name(file_suffix)
		"#{@output_dir}\\output_#{file_suffix}.txt"
	end
end
