class OpenBabelHelper
	def self.convert(input_string, input_format, output_format)
		random_string = ('a'..'z').to_a.shuffle[0,8].join
    temp_file = "obabel_temp\\openbabel_temporary_file_#{random_string}.txt"
		File.open(temp_file, "w") { |f| f.write input_string.gsub("\r\n", "\n") }
		
		`\"programs\\openbabel\\obabel.exe\" -i#{input_format} #{temp_file} -o#{output_format} 2>nul`[0..-3]
  end

  def self.convert_standard(input_string, input_format)
  	convert(input_string, input_format, "can").upcase.gsub("/", "").gsub("\\", "")
  end

  def self.get_mass(input_string, input_format)
		report = convert input_string, input_format, "report"
		report.scan(/EXACT\sMASS:\s(.*)$/)[0][0].to_f
  end

  def self.get_formula(input_string, input_format)
    report = convert input_string, input_format, "report"
    report.scan(/FORMULA:\s(.*)$/)[0][0]
  end

  def self.remove_charge(input_string, input_format)
  	temp = convert input_string, input_format, "mol2"
  	result = convert temp, "mol2", input_format
  	result.gsub "	****", ""
  end
end
