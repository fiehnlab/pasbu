class QualitativeOverlapAnalysisModule
	def self.analyze(spectra, programs)
		# hash initialization
		program_connection_results = { :all => {:sum => 0, :share => 0.0}, :none => {:sum => 0, :share => 0.0} }
		programs.each do |program1|
			program_connection_results[program1.to_sym] = {}
			programs.each do |program2|
				program_connection_results[program1.to_sym][program2.to_sym] = {}
				program_connection_results[program1.to_sym][program2.to_sym][:sum] = 0
				program_connection_results[program1.to_sym][program2.to_sym][:share] = 0.0
			end
		end

		# counting
		peak_count = 0
		spectra.each do |spectrum|
			spectrum[:peaks].each do |peak|
				if peak[:mz]
					peak_count += 1
					all = true
					none = true
					
					programs.each do |program1|
						if peak[:structure][program1.to_sym]
							none = false
							programs.each do |program2|
								if peak[:structure][program1.to_sym] == peak[:structure][program2.to_sym]
									program_connection_results[program1.to_sym][program2.to_sym][:sum] += 1
								else
									all = false
								end
							end
						else
							all = false
						end
					end

					program_connection_results[:all][:sum] += 1 if all
					program_connection_results[:none][:sum] += 1 if none
				end
			end
		end

		# final calculation
		programs.each do |program1|
			programs.each do |program2|
				program_connection_results[program1.to_sym][program2.to_sym][:share] = program_connection_results[program1.to_sym][program2.to_sym][:sum].to_f / program_connection_results[program2.to_sym][program2.to_sym][:sum]
			end
		end
		program_connection_results[:all][:share] = program_connection_results[:all][:sum].to_f / peak_count
		program_connection_results[:none][:share] = program_connection_results[:none][:sum].to_f / peak_count

		# result output
		result_string = "-- Comparison Results --\n"
		programs.each do |program1|
			result_string += "#{program1} annotated peaks of program X annotated peaks (#{programs.join ' '}):	"
			programs.each do |program2|
				result_string += "#{(program_connection_results[program1.to_sym][program2.to_sym][:share] * 100).round 1}%	"
			end
			result_string += "\n"
		end
		result_string += "Share of peaks all programs annotated: #{(program_connection_results[:all][:share] * 100).round 1}%\n"
		result_string += "Share of peaks no programs annotated: #{(program_connection_results[:none][:share] * 100).round 1}%\n"
		result_string += "-- \\Comparison Results\\ --"

		result_string
	end
end
